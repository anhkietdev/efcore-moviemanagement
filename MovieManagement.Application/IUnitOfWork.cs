﻿using MovieManagement.Application.Contracts;

namespace MovieManagement.Application
{
    public interface IUnitOfWork
    {
        IMovieRepository Movie { get; }
        IReviewRepository Review { get; }
        void Save();
    }
}
