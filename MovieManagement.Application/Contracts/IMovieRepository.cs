﻿using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Contracts
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
    }
}
