﻿using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Services.Interfaces
{
    public interface IMovieService
    {
        IEnumerable<Movie> GetMovies();
        Movie GetMovieById(int movieId);
        void AddMovie(Movie movie);
        void UpdateMovie(Movie updatedMovie);
        void DeleteMovie(int movieId);
        IEnumerable<Movie> GetMoviesByName(string name);
        IEnumerable<Movie> GetMoviesByAuthor(string author);
        IEnumerable<Movie> GetMoviesByCountry(string country);
        IEnumerable<Movie> GetMoviesByPublicationYear(int year);
    }
}
