﻿using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Services.Interfaces
{
    public interface IReviewService
    {
        IEnumerable<Review> GetReviews(int movieId);
        void AddReview(Review review);

    }
}
