﻿using MovieManagement.Application.Services.Interfaces;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Services.Implements
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddMovie(Movie movie)
        {
            try
            {
                _unitOfWork.Movie.Add(movie);
                _unitOfWork.Save();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteMovie(int movieId)
        {
            try
            {
                Movie movieToDelete = _unitOfWork.Movie.Get(movieId);

                if (movieToDelete is not null)
                {
                    _unitOfWork.Movie.Remove(movieToDelete);
                    _unitOfWork.Save();
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Movie GetMovieById(int movieId)
        {
            try
            {
                Movie movie = _unitOfWork.Movie.Get(movieId);

                return movie;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Movie> GetMovies()
        {
            try
            {
                IEnumerable<Movie> movies = _unitOfWork.Movie.GetAll();
                return movies;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateMovie(Movie updatedMovie)
        {
            try
            {
                Movie movieToUpdate = _unitOfWork.Movie.Get(updatedMovie.Id);
                if (movieToUpdate is not null)
                {
                    movieToUpdate.Name = updatedMovie.Name;
                    movieToUpdate.Author = updatedMovie.Author;
                    movieToUpdate.Country = updatedMovie.Country;
                    movieToUpdate.PublicationYear = updatedMovie.PublicationYear;

                    _unitOfWork.Movie.Update(movieToUpdate);
                    _unitOfWork.Save();
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<Movie> GetMoviesByAuthor(string author)
        {
            return _unitOfWork.Movie.Find(x => x.Name == author);
        }

        public IEnumerable<Movie> GetMoviesByCountry(string country)
        {
            return _unitOfWork.Movie.Find(x => x.Country == country);
        }

        public IEnumerable<Movie> GetMoviesByName(string name)
        {
            return _unitOfWork.Movie.Find(x => x.Name == name);

        }

        public IEnumerable<Movie> GetMoviesByPublicationYear(int year)
        {
            return _unitOfWork.Movie.Find(x => x.PublicationYear == year);
        }
    }
}
