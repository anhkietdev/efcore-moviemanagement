﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Application.Contracts;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Infrastructure.Repositories
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<Movie> GetMoviesByName(string name) => _dbSet.Where(m => m.Name == name).ToList();

        public IEnumerable<Movie> GetMoviesByAuthor(string author) => _dbSet.Where(m => m.Author == author).ToList();

        public IEnumerable<Movie> GetMoviesByCountry(string country) => _dbSet.Where(m => m.Country == country).ToList();

        public IEnumerable<Movie> GetMoviesByPublicationYear(int year) => _dbSet.Where(m => m.PublicationYear == year).ToList();
    }
}
