﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Application.Contracts;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Infrastructure.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(DbContext context) : base(context)
        {
        }
    }
}
