﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Infrastructure
{
    public class AppDbContext : DbContext
    {
        
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=KIETTA;Database=MovieManagement;Trusted_Connection=True;TrustServerCertificate=True;User Id=kietta;Password=123";
            optionsBuilder.UseSqlServer(connect);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                        .HasMany(m => m.Reviews)
                        .WithOne(r => r.Movie)
                        .HasForeignKey(r => r.MovieId);
        }
    }
}
