﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MovieManagement.Infrastructure
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var optionBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionBuilder.UseSqlServer("Data Source=KIETTA;Database=MovieManagement;Trusted_Connection=True;TrustServerCertificate=True;User Id=kietta;Password=123");
            return new AppDbContext(optionBuilder.Options);
        }
    }
}
