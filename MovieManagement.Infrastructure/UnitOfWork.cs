﻿using MovieManagement.Application.Contracts;
using MovieManagement.Application;
using MovieManagement.Infrastructure.Repositories;

namespace MovieManagement.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _db;

        public IMovieRepository Movie { get; set; }
        public IReviewRepository Review { get; set; }

        public UnitOfWork(AppDbContext db)
        {
            _db = db;
            Movie = new MovieRepository(_db);
            Review = new ReviewRepository(_db);
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
