﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MovieManagement.Application;
using MovieManagement.Application.Services.Implements;
using MovieManagement.Application.Services.Interfaces;
using MovieManagement.Infrastructure;
using MovieManagement.Presentation;
using MovieManagement.Presentation.Presentation;

IHostBuilder hostBuilder = Host.CreateDefaultBuilder();

hostBuilder.ConfigureServices((hostContext, services) =>
{
    services.AddDbContext<AppDbContext>(options =>
    {
        options.UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"))
               .UseLoggerFactory(LoggerFactory.Create(builder => builder.AddFilter((category, level) => level >= LogLevel.Warning)));
    });


    services.AddScoped<MainApplication>();

    services.AddScoped<IUnitOfWork, UnitOfWork>();

    services.AddScoped<IMovieService, MovieService>();
    services.AddScoped<IReviewService, ReviewService>();

    services.AddScoped<IMoviePresentation, MoviePresentation>();
});

IHost host = hostBuilder.Build();

var app = host.Services.GetRequiredService<MainApplication>();

app.Run();