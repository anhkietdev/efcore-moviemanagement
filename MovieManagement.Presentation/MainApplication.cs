﻿using MovieManagement.Presentation.Presentation;
using MovieManagement.Presentation.Utils;

namespace MovieManagement.Presentation
{
    public class MainApplication
    {
        private readonly IMoviePresentation _moviePresentation;

        public MainApplication(IMoviePresentation moviePresentation)
        {
            _moviePresentation = moviePresentation;
        }

        public void Run()
        {
            try
            {
                
                int choice;
                do
                {
                    MenuUtils.MainMenu();
                    choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            _moviePresentation.ViewMovies();
                            break;
                        case 2:
                            _moviePresentation.AddMovie();
                            break;
                        case 3:
                            _moviePresentation.SearchMovie();
                            break;
                        default:
                            break;
                    }
                } while (choice != 4);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
