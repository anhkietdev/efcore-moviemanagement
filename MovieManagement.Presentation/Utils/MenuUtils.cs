﻿namespace MovieManagement.Presentation.Utils
{
    public static class MenuUtils
    {
        internal const string USER_CHOICE = "Your choice: ";
        internal const string INVALID_CHOICE = "Invalid choice!";
        internal const string EXIT = "Exit";
        internal const string BACK_TO_MAIN_MENU = "Back to Main Menu";
        internal const string PRESS_ANY_KEY_TO_CONT = "Press Any Key To Continue ...";
        internal const string NOT_FOUND = "Not found!";
        internal const string ERROR = "Error: ";

        internal const string MAINMENU = "Movie Management";

        internal const string VIEW_MOVIES = "View Movies";
        internal const string VIEW_MOVIES_DETAIL = "View Movies Detail";

        
        internal const string ADD = "Add Movie";
        internal const string ADD_SUCCESSFULLY = "Added successfully!";
        internal const string UPDATE = "Update Movie";
        internal const string DELETE = "Delete Movie";
        internal const string SEARCH = "Search Movie";

        internal const string MOVIE_INFO = "Movie Information:";
        internal const string MOVIE_ID = "Id: ";
        internal const string MOVIE_NAME = "Name: ";
        internal const string MOVIE_AUTHOR = "Author: ";
        internal const string MOVIE_PUBYEAR = "Publication Year: ";
        internal const string MOVIE_COUNTRY = "Country: ";

        internal const string ADD_REVIEWS = "Add Review";
        internal const string VIEW_REVIEWS = "View Reviews";

        internal const string SEARCH_MOVIE_BY_NAME = "Search Movies By Name";
        internal const string SEARCH_MOVIE_BY_PUBLICATION_YEAR = "Search Movies By Publication Year";
        internal const string SEARCH_MOVIE_BY_AUTHOR = "Search Movies By Director Name";
        internal const string SEARCH_MOVIE_BY_COUNTRY = "Search Movies By Country Name";

        internal const string ENTER_NAME = "Enter Name: ";
        internal const string ENTER_AUTHOR = "Enter Author: ";
        internal const string ENTER_PUBLICATION_YEAR = "Enter Publication Year: ";
        internal const string ENTER_COUNTRY = "Enter Country: ";

        internal const string UPDATE_NAME = "Enter New Name: ";
        internal const string UPDATE_AUTHOR = "Enter New Author: ";
        internal const string UPDATE_PUBLICATION_YEAR = "Enter New Publication Year: ";
        internal const string UPDATE_COUNTRY = "Enter New Country: ";
        internal const string UPDATE_SUCCESSFULLY = "Updated successfully!";

        internal const string DELETED_SUCCESSFULLY = "Deleted successfully!";

        internal const string REVIEW_ID = "Id: ";
        internal const string REVIEW_TITLE = "Title: ";
        internal const string REVIEW_CONTENT = "Content: ";
        internal const string REVIEW_RATING = "Rating: ";

        internal const string ADD_REVIEW_TITLE = "Enter Title: ";
        internal const string ADD_REVIEW_CONTENT = "Enter Content: ";
        internal const string ADD_REVIEW_RATING = "Enter Rating (1 to 5): ";
        internal const string ADD_REVIEW_RATING_REQUIRE = "Rating must be between 1 and 5.";
        internal const string ADD_REVIEW_SUCCESSFULLY = "Review added successfully!";

        public static void MainMenu()
        {
            Console.WriteLine($"\t[{MAINMENU}]");
            Console.WriteLine();

            Console.WriteLine($"\t1 - {VIEW_MOVIES}");
            Console.WriteLine($"\t2 - {ADD}");
            Console.WriteLine($"\t3 - {SEARCH}");
            Console.WriteLine($"\t4 - {EXIT}");
            Console.Write($"\t{USER_CHOICE}");

        }

        public static void SubMenu()
        {
            Console.WriteLine($"\t1 - {UPDATE}");
            Console.WriteLine($"\t2 - {DELETE}");
            Console.WriteLine($"\t3 - {ADD_REVIEWS}");
            Console.WriteLine($"\t4 - {VIEW_REVIEWS}");
            Console.WriteLine($"\t5 - {BACK_TO_MAIN_MENU}");
            Console.Write($"\t{USER_CHOICE}");
        }

        public static void SearchMenu()
        {
            Console.WriteLine($"\t1 - {SEARCH_MOVIE_BY_NAME}");
            Console.WriteLine($"\t2 - {SEARCH_MOVIE_BY_AUTHOR}");
            Console.WriteLine($"\t3 - {SEARCH_MOVIE_BY_PUBLICATION_YEAR}");
            Console.WriteLine($"\t4 - {SEARCH_MOVIE_BY_COUNTRY}");
            Console.WriteLine($"\t5 - {BACK_TO_MAIN_MENU}");
            Console.Write($"\t{USER_CHOICE}");
        }

        public static void PressAnyKeyToContinue()
        {
            Console.WriteLine(MenuUtils.PRESS_ANY_KEY_TO_CONT);
            Console.ReadKey(intercept: true);
        }
    }
}
