﻿using MovieManagement.Application.Services.Interfaces;
using MovieManagement.Domain.Entities;
using MovieManagement.Presentation.Utils;

namespace MovieManagement.Presentation.Presentation
{
    public class MoviePresentation : IMoviePresentation
    {
        private readonly IMovieService _movieService;
        private readonly IReviewService _reviewService;

        private int currentPage = 1;
        private int pageSize = 5;
        private int totalPages;

        public MoviePresentation(IMovieService movieService, IReviewService reviewService)
        {
            _movieService = movieService;
            _reviewService = reviewService;
        }

        public void AddMovie()
        {
            try
            {
                Movie movie = new Movie();
                Console.WriteLine(MenuUtils.ADD);

                Console.Write(MenuUtils.MOVIE_NAME);
                movie.Name = Console.ReadLine();
                Console.Write(MenuUtils.MOVIE_AUTHOR);
                movie.Author = Console.ReadLine();
                Console.Write(MenuUtils.MOVIE_PUBYEAR);
                movie.PublicationYear = Convert.ToInt32(Console.ReadLine());
                Console.Write(MenuUtils.MOVIE_COUNTRY);
                movie.Country = Console.ReadLine();

                _movieService.AddMovie(movie);
                Console.WriteLine(MenuUtils.ADD_SUCCESSFULLY);
                MenuUtils.PressAnyKeyToContinue();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SearchMovie()
        {
            try
            {
                int choice;
                do
                {
                    Console.WriteLine($"\t1 - {MenuUtils.SEARCH_MOVIE_BY_NAME}");
                    Console.WriteLine($"\t2 - {MenuUtils.SEARCH_MOVIE_BY_AUTHOR}");
                    Console.WriteLine($"\t3 - {MenuUtils.SEARCH_MOVIE_BY_COUNTRY}");
                    Console.WriteLine($"\t4 - {MenuUtils.SEARCH_MOVIE_BY_PUBLICATION_YEAR}");
                    Console.WriteLine($"\t5 - {MenuUtils.BACK_TO_MAIN_MENU}");
                    Console.Write($"\t{MenuUtils.USER_CHOICE}");

                    choice = Convert.ToInt32(Console.ReadLine());

                    switch (choice)
                    {
                        case 1:
                            Console.Write($"{MenuUtils.ENTER_NAME}");
                            string name = Console.ReadLine();
                            SearchByName(name);
                            break;
                        case 2:
                            Console.Write($"{MenuUtils.ENTER_AUTHOR}");
                            string author = Console.ReadLine();
                            SearchByAuthor(author);
                            break;
                        case 3:
                            Console.Write($"{MenuUtils.ENTER_COUNTRY}");
                            string country = Console.ReadLine();
                            SearchByCountry(country);
                            break;
                        case 4:
                            Console.Write($"{MenuUtils.ENTER_PUBLICATION_YEAR}");
                            int year = Convert.ToInt32(Console.ReadLine());
                            SearchByPublicationYear(year);
                            break;
                        case 5:
                            break;
                        default:
                            Console.WriteLine(MenuUtils.INVALID_CHOICE);
                            break;
                    }
                } while (choice != 5);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void ViewMovies()
        {
            try
            {
                var totalMovies = _movieService.GetMovies().Count();

                totalPages = (int)Math.Ceiling((double)totalMovies / pageSize);

                var movies = _movieService.GetMovies()
                    .Skip((currentPage - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();

                Console.WriteLine("Movies on Page " + currentPage + " of " + totalPages);
                foreach (var movie in movies)
                {
                    Console.WriteLine($"\tID: {movie.Id}");
                    Console.WriteLine($"\tName: {movie.Name}");
                    Console.WriteLine($"\tAuthor: {movie.Author}");
                    Console.WriteLine($"\tPublication Year: {movie.PublicationYear}");
                    Console.WriteLine($"\tCountry: {movie.Country}");
                    Console.WriteLine();
                }

                Console.WriteLine("[N] - Next Page\t\t[P] - Previous Page\t\t[M] - View Movie Detail\t\t[B] - Back to Main Menu");
                var input = Console.ReadLine();
                HandlePageNavigation(input);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        private void HandlePageNavigation(string input)
        {
            if (input.Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                if (currentPage < totalPages)
                {
                    Console.Clear();
                    currentPage++;
                    ViewMovies();
                }
            }
            else if (input.Equals("P", StringComparison.OrdinalIgnoreCase))
            {
                if (currentPage > 1)
                {
                    Console.Clear();
                    currentPage--;
                    ViewMovies();
                }
            }
            else if (input.Equals("M", StringComparison.OrdinalIgnoreCase))
            {
                Console.Write("Enter Id: ");
                int id = Convert.ToInt32(Console.ReadLine());
                var movie = _movieService.GetMovieById(id);

                if (movie != null)
                {
                    int choice;

                    do
                    {
                        Console.Clear();
                        Console.WriteLine($"\t{MenuUtils.MOVIE_INFO}");
                        Console.WriteLine();
                        Console.WriteLine($"\t{MenuUtils.MOVIE_ID}: {movie.Id}");
                        Console.WriteLine($"\t{MenuUtils.MOVIE_NAME}: {movie.Name}");
                        Console.WriteLine($"\t{MenuUtils.MOVIE_AUTHOR}: {movie.Author}");
                        Console.WriteLine($"\t{MenuUtils.MOVIE_PUBYEAR}: {movie.PublicationYear}");
                        Console.WriteLine($"\t{MenuUtils.MOVIE_COUNTRY}: {movie.Country}");
                        Console.WriteLine();

                        MenuUtils.SubMenu();
                        choice = Convert.ToInt32(Console.ReadLine());

                        switch (choice)
                        {
                            case 1:
                                UpdateMovie(movie);
                                break;
                            case 2:
                                DeleteMovie(movie);
                                break;
                            case 3:
                                AddReview(movie);
                                break;
                            case 4:
                                GetAllReviews(id);
                                break;
                            case 5:
                                break;
                            default:
                                Console.WriteLine(MenuUtils.INVALID_CHOICE);
                                break;
                        }
                    } while (choice != 5);
                }
            }
            else if (input.Equals("B", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
        }

        private void SearchByName(string name)
        {
            IEnumerable<Movie> movies = _movieService.GetMoviesByName(name).ToList();
            if (movies is not null)
            {
                foreach (var item in movies)
                {
                    Console.WriteLine();
                    Console.WriteLine($"\tInformation of Movie {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_ID}: {item.Id}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_NAME}: {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_AUTHOR}: {item.Author}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_PUBYEAR}: {item.PublicationYear}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_COUNTRY}: {item.Country}");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine(MenuUtils.NOT_FOUND);
            }
        }

        private void SearchByAuthor(string author)
        {
            IEnumerable<Movie> movies = _movieService.GetMoviesByAuthor(author).ToList();
            if (movies is not null)
            {
                foreach (var item in movies)
                {
                    Console.WriteLine();
                    Console.WriteLine($"\tInformation of Movie {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_ID}: {item.Id}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_NAME}: {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_AUTHOR}: {item.Author}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_PUBYEAR}: {item.PublicationYear}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_COUNTRY}: {item.Country}");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine(MenuUtils.NOT_FOUND);
            }
        }

        private void SearchByPublicationYear(int year)
        {
            IEnumerable<Movie> movies = _movieService.GetMoviesByPublicationYear(year).ToList();
            if (movies is not null)
            {
                foreach (var item in movies)
                {
                    Console.WriteLine();
                    Console.WriteLine($"\tInformation of Movie {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_ID}: {item.Id}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_NAME}: {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_AUTHOR}: {item.Author}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_PUBYEAR}: {item.PublicationYear}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_COUNTRY}: {item.Country}");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine(MenuUtils.NOT_FOUND);
            }
        }

        private void SearchByCountry(string country)
        {
            IEnumerable<Movie> movies = _movieService.GetMoviesByAuthor(country).ToList();
            if (movies is not null)
            {
                foreach (var item in movies)
                {
                    Console.WriteLine();
                    Console.WriteLine($"\tInformation of Movie {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_ID}: {item.Id}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_NAME}: {item.Name}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_AUTHOR}: {item.Author}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_PUBYEAR}: {item.PublicationYear}");
                    Console.WriteLine($"\t{MenuUtils.MOVIE_COUNTRY}: {item.Country}");
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine(MenuUtils.NOT_FOUND);
            }

        }

        private void UpdateMovie(Movie movie)
        {
            try
            {
                Console.Write(MenuUtils.UPDATE_NAME);
                movie.Name = Console.ReadLine();
                Console.Write(MenuUtils.UPDATE_AUTHOR);
                movie.Author = Console.ReadLine();
                Console.Write(MenuUtils.UPDATE_PUBLICATION_YEAR);
                movie.PublicationYear = Convert.ToInt32(Console.ReadLine());
                Console.Write(MenuUtils.UPDATE_COUNTRY);
                movie.Country = Console.ReadLine();

                _movieService.UpdateMovie(movie);
                Console.WriteLine(MenuUtils.UPDATE_SUCCESSFULLY);
                MenuUtils.PressAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                Console.WriteLine(MenuUtils.ERROR + ex.Message);
            }
        }

        private void DeleteMovie(Movie movie)
        {
            try
            {
                _movieService.DeleteMovie(movie.Id);
                Console.WriteLine(MenuUtils.DELETED_SUCCESSFULLY);
                MenuUtils.PressAnyKeyToContinue();
            }
            catch (Exception ex)
            {
                Console.WriteLine(MenuUtils.ERROR + ex.Message);
            }
        }

        private void GetAllReviews(int movieId)
        {
            IEnumerable<Review> reviews = _reviewService.GetReviews(movieId);
            foreach (var item in reviews)
            {
                Console.WriteLine();
                Console.WriteLine($"\tAll Reviews of Movie {item.Movie.Name}");
                Console.WriteLine($"\t\t{MenuUtils.REVIEW_ID}: {item.Id}");
                Console.WriteLine($"\t\t{MenuUtils.REVIEW_TITLE}: {item.Title}");
                Console.WriteLine($"\t\t{MenuUtils.REVIEW_CONTENT}: {item.Content}");
                Console.WriteLine($"\t\t{MenuUtils.REVIEW_RATING}: {item.Rating}");
                Console.WriteLine();
            }
            MenuUtils.PressAnyKeyToContinue();
        }
        private void AddReview(Movie movie)
        {
            try
            {
                Review review = new Review();

                Console.Write(MenuUtils.ADD_REVIEW_TITLE);
                review.Title = Console.ReadLine();
                Console.Write(MenuUtils.ADD_REVIEW_CONTENT);
                review.Content = Console.ReadLine();

                int rating;
                do
                {
                    Console.Write(MenuUtils.ADD_REVIEW_RATING);
                    if (int.TryParse(Console.ReadLine(), out rating) && rating >= 1 && rating <= 5)
                    {
                        review.Rating = rating;
                    }
                    else
                    {
                        Console.WriteLine(MenuUtils.ADD_REVIEW_RATING_REQUIRE);
                    }
                } while (rating < 1 || rating > 5);

                review.MovieId = movie.Id;

                Console.WriteLine(MenuUtils.ADD_REVIEW_SUCCESSFULLY);
                MenuUtils.PressAnyKeyToContinue();
                _reviewService.AddReview(review);

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
