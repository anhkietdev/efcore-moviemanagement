using Moq;
using MovieManagement.Application;
using MovieManagement.Application.Services.Implements;
using MovieManagement.Domain.Entities;
using System.Linq.Expressions;

public class MovieServiceTests
{
    private MovieService movieService;
    private Mock<IUnitOfWork> unitOfWorkMock;

    public MovieServiceTests()
    {
        unitOfWorkMock = new Mock<IUnitOfWork>();
        movieService = new MovieService(unitOfWorkMock.Object);
    }

    [Fact]
    public void AddMovie_ValidMovie_AddsMovieToDatabase()
    {
        // Arrange
        var movie = new Movie
        {
            Name = "Test Movie",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        };

        unitOfWorkMock.Setup(u => u.Movie.Add(It.IsAny<Movie>()));
        unitOfWorkMock.Setup(u => u.Save());

        // Act
        movieService.AddMovie(movie);

        // Assert
        unitOfWorkMock.Verify(u => u.Movie.Add(movie), Times.Once);
        unitOfWorkMock.Verify(u => u.Save(), Times.Once);
    }

    [Fact]
    public void DeleteMovie_ExistingMovie_DeletesMovieFromDatabase()
    {
        // Arrange
        int movieId = 1;
        var existingMovie = new Movie
        {
            Id = movieId,
            Name = "Test Movie",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        };

        unitOfWorkMock.Setup(u => u.Movie.Get(movieId)).Returns(existingMovie);
        unitOfWorkMock.Setup(u => u.Movie.Remove(existingMovie));
        unitOfWorkMock.Setup(u => u.Save());

        // Act
        movieService.DeleteMovie(movieId);

        // Assert
        unitOfWorkMock.Verify(u => u.Movie.Get(movieId), Times.Once);
        unitOfWorkMock.Verify(u => u.Movie.Remove(existingMovie), Times.Once);
        unitOfWorkMock.Verify(u => u.Save(), Times.Once);
    }

    [Fact]
    public void UpdateMovie_ExistingMovie_UpdatesMovieInDatabase()
    {
        // Arrange
        var updatedMovie = new Movie
        {
            Id = 1,
            Name = "Updated Movie",
            Author = "Updated Author",
            Country = "Updated Country",
            PublicationYear = 2024
        };

        var existingMovie = new Movie
        {
            Id = 1,
            Name = "Test Movie",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        };

        unitOfWorkMock.Setup(u => u.Movie.Get(updatedMovie.Id)).Returns(existingMovie);

        // Act
        movieService.UpdateMovie(updatedMovie);

        // Assert
        unitOfWorkMock.Verify(u => u.Movie.Get(updatedMovie.Id), Times.Once);
        Assert.Equal(updatedMovie.Name, existingMovie.Name);
        Assert.Equal(updatedMovie.Author, existingMovie.Author);
        Assert.Equal(updatedMovie.Country, existingMovie.Country);
        Assert.Equal(updatedMovie.PublicationYear, existingMovie.PublicationYear);
        unitOfWorkMock.Verify(u => u.Movie.Update(existingMovie), Times.Once);
        unitOfWorkMock.Verify(u => u.Save(), Times.Once);
    }

    [Fact]
    public void GetAllMovies_ExistingMovies_ReturnsAllMovies()
    {
        // Arrange
        var movies = new List<Movie>
    {
        new Movie
        {
            Id = 1,
            Name = "Movie 1",
            Author = "Test Author 1",
            Country = "Test Country 1",
            PublicationYear = 2023
        },
        new Movie
        {
            Id = 2,
            Name = "Movie 2",
            Author = "Test Author 2",
            Country = "Test Country 2",
            PublicationYear = 2024
        }
    };

        unitOfWorkMock.Setup(u => u.Movie.GetAll()).Returns(movies);

        // Act
        var result = movieService.GetMovies();

        // Assert
        Assert.NotEmpty(result);
        Assert.Equal(movies.Count, result.Count());
    }

    [Fact]
    public void GetMovieById_ExistingMovie_ReturnsMovie()
    {
        // Arrange
        int movieId = 1;
        var existingMovie = new Movie
        {
            Id = movieId,
            Name = "Test Movie",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        };

        unitOfWorkMock.Setup(u => u.Movie.Get(movieId)).Returns(existingMovie);

        // Act
        var result = movieService.GetMovieById(movieId);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(movieId, result.Id);
    }

    [Fact]
    public void GetMoviesByAuthor_ExistingAuthor_ReturnsMovies()
    {
        // Arrange
        string author = "Test Author";
        var movies = new List<Movie>
    {
        new Movie
        {
            Name = "Movie 1",
            Author = author,
            Country = "Test Country",
            PublicationYear = 2023
        },
        new Movie
        {
            Name = "Movie 2",
            Author = author,
            Country = "Test Country",
            PublicationYear = 2024
        }
    };

        unitOfWorkMock.Setup(u => u.Movie.Find(It.IsAny<Expression<Func<Movie, bool>>>())).Returns(movies);

        // Act
        var result = movieService.GetMoviesByAuthor(author);

        // Assert
        Assert.NotEmpty(result);
        Assert.All(result, movie => Assert.Equal(author, movie.Author));
    }

    [Fact]
    public void GetMoviesByCountry_ExistingCountry_ReturnsMovies()
    {
        // Arrange
        string country = "Test Country";
        var movies = new List<Movie>
    {
        new Movie
        {
            Name = "Movie 1",
            Author = "Test Author",
            Country = country,
            PublicationYear = 2023
        },
        new Movie
        {
            Name = "Movie 2",
            Author = "Another Author",
            Country = country,
            PublicationYear = 2024
        }
    };

        unitOfWorkMock.Setup(u => u.Movie.Find(It.IsAny<Expression<Func<Movie, bool>>>())).Returns(movies);

        // Act
        var result = movieService.GetMoviesByCountry(country);

        // Assert
        Assert.NotEmpty(result);
        Assert.All(result, movie => Assert.Equal(country, movie.Country));
    }

    [Fact]
    public void GetMoviesByName_ExistingName_ReturnsMovies()
    {
        // Arrange
        string name = "Test Movie";
        var movies = new List<Movie>
    {
        new Movie
        {
            Name = name,
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        },
        new Movie
        {
            Name = name,
            Author = "Another Author",
            Country = "Another Country",
            PublicationYear = 2024
        }
    };

        unitOfWorkMock.Setup(u => u.Movie.Find(It.IsAny<Expression<Func<Movie, bool>>>())).Returns(movies);

        // Act
        var result = movieService.GetMoviesByName(name);

        // Assert
        Assert.NotEmpty(result);
        Assert.All(result, movie => Assert.Equal(name, movie.Name));
    }

    [Fact]
    public void GetMoviesByPublicationYear_ExistingYear_ReturnsMovies()
    {
        // Arrange
        int year = 2023;
        var movies = new List<Movie>
    {
        new Movie
        {
            Name = "Movie 1",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = year
        },
        new Movie
        {
            Name = "Movie 2",
            Author = "Another Author",
            Country = "Another Country",
            PublicationYear = year
        }
    };

        unitOfWorkMock.Setup(u => u.Movie.Find(It.IsAny<Expression<Func<Movie, bool>>>())).Returns(movies);

        // Act
        var result = movieService.GetMoviesByPublicationYear(year);

        // Assert
        Assert.NotEmpty(result);
        Assert.All(result, movie => Assert.Equal(year, movie.PublicationYear));
    }
}
