﻿using Moq;
using MovieManagement.Application;
using MovieManagement.Application.Services.Implements;
using MovieManagement.Domain.Entities;

public class ReviewServiceTests
{
    private ReviewService reviewService;
    private Mock<IUnitOfWork> unitOfWorkMock;

    public ReviewServiceTests()
    {
        unitOfWorkMock = new Mock<IUnitOfWork>();
        reviewService = new ReviewService(unitOfWorkMock.Object);
    }

    [Fact]
    public void GetReviews_ValidMovieId_ReturnsReviews()
    {
        // Arrange
        int movieId = 1;
        var reviews = new List<Review>
        {
            new Review
            {
                Id = 1,
                Title = "Review 1",
                Content = "Test review 1",
                Rating = 4,
                MovieId = movieId
            },
            new Review
            {
                Id = 2,
                Title = "Review 2",
                Content = "Test review 2",
                Rating = 5,
                MovieId = movieId
            }
        };

        unitOfWorkMock.Setup(u => u.Review.GetAll()).Returns(reviews);

        // Act
        var result = reviewService.GetReviews(movieId);

        // Assert
        Assert.NotEmpty(result);
        Assert.Equal(reviews.Count, result.Count());
    }

    [Fact]
    public void AddReview_ValidReview_AddsReviewToDatabase()
    {
        // Arrange
        int movieId = 1;
        var review = new Review
        {
            Title = "Test Review",
            Content = "Test content",
            Rating = 5,
            MovieId = movieId
        };

        var movieToAddReview = new Movie
        {
            Id = movieId,
            Name = "Test Movie",
            Author = "Test Author",
            Country = "Test Country",
            PublicationYear = 2023
        };

        unitOfWorkMock.Setup(u => u.Movie.Get(movieId)).Returns(movieToAddReview);
        unitOfWorkMock.Setup(u => u.Review.Add(It.IsAny<Review>()));
        unitOfWorkMock.Setup(u => u.Save());

        // Act
        reviewService.AddReview(review);

        // Assert
        unitOfWorkMock.Verify(u => u.Movie.Get(movieId), Times.Once);
        Assert.NotNull(review.Movie);
        Assert.Equal(movieToAddReview, review.Movie);
        unitOfWorkMock.Verify(u => u.Review.Add(review), Times.Once);
        unitOfWorkMock.Verify(u => u.Save(), Times.Once);
    }
}
