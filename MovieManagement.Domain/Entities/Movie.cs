﻿namespace MovieManagement.Domain.Entities
{
    public class Movie
    {
        private string _name;
        private string _author;
        private string _country;
        private int _publicationYear;

        public int Id { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new ArgumentException("Name must not be empty and should not exceed 255 characters.");
                }
                _name = value;
            }
        }

        public string Author
        {
            get => _author;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new ArgumentException("Author must not be empty and should not exceed 255 characters.");
                }
                _author = value;
            }
        }

        public string Country
        {
            get => _country;
            set
            {
                if (string.IsNullOrWhiteSpace(value) || value.Length > 255)
                {
                    throw new ArgumentException("Country must not be empty and should not exceed 255 characters.");
                }
                _country = value;
            }
        }

        public int PublicationYear
        {
            get => _publicationYear;
            set
            {
                if (value <= 1888)
                {
                    throw new ArgumentException("Publication year must be greater than 1888.");
                }
                _publicationYear = value;
            }
        }

        public ICollection<Review> Reviews { get; set; }
    }

}
