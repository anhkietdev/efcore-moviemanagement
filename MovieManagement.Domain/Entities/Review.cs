﻿namespace MovieManagement.Domain.Entities
{
    public class Review
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        private int _rating;

        public int Rating
        {
            get { return _rating; }
            set
            {
                if (value < 1 || value > 5)
                {
                    throw new ArgumentException("Rating must be between 1 and 5.");
                }
                _rating = value;
            }
        }

        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }

}
